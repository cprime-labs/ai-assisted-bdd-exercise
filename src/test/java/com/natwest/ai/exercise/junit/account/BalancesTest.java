package com.natwest.ai.exercise.junit.account;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.natwest.ai.exercise.account.Balances;
import java.io.IOException;
import org.junit.jupiter.api.Test;

/** Unit tests for Balances class. */
@SuppressWarnings({"PMD.AtLeastOneConstructor", "PMD.CommentSize"})
class BalancesTest {

  /*Test getAccessToken method.*/
  @Test
  void getAccessTokenTest() throws IOException {

    assertNotNull(new Balances().getAccessToken(), "Access Token is Null.");
  }

  /* Test getContent method.*/
  @Test
  void getContentTest() throws IOException {
    assertNotNull(new Balances().getContent(), "Content is Null.");
  }
}
