package com.natwest.ai.exercise.junit;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.natwest.ai.exercise.Executable;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import org.junit.jupiter.api.Test;

/** Unit tests for Executable class. */
@SuppressWarnings({"PMD.AtLeastOneConstructor", "PMD.CommentSize"})
class ExecutableTest {

  @Test
  void contructorTest() {

    assertNotNull(new Executable(), "Executable constructor returned Null");
  }

  @Test
  void retrieveAccessTokenTest() {

    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

    PrintStream out = new PrintStream(byteArrayOutputStream);

    System.setOut(out);

    Executable.main(null);

    String consoleOutput = byteArrayOutputStream.toString(Charset.defaultCharset());

    assertFalse(consoleOutput.isEmpty(), "Console output is empty.");

    out.close();
  }

}
