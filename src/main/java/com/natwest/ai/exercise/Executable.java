package com.natwest.ai.exercise;

import com.natwest.ai.exercise.account.Balances;
import com.natwest.ai.exercise.common.ConsoleWriter;

/** Executable class to act as main entrypoint for the solution to the AI BDD exercise brief. */
@SuppressWarnings({"PMD.UseUtilityClass", "PMD.CommentSize", "PMD.UnnecessaryConstructor"})
public class Executable {

  /** Default constructor for unit test coverage. */
  public Executable() {
    // Default constructor.
  }

  /**
   * Main method for the executable.
   *
   * @param args command line arguments passed to the main method.
   */
  public static void main(final String[] args) {

    final ConsoleWriter consoleWriter = new ConsoleWriter();

    consoleWriter.writeAccountInformation(new Balances());
  }
}
