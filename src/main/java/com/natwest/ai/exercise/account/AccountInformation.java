package com.natwest.ai.exercise.account;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.natwest.ai.exercise.common.ApplicationProperties;
import com.natwest.ai.exercise.dto.AccessToken;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/** Base class implementing common functionality for Account Information ojects. */
@SuppressWarnings({
  "PMD.AtLeastOneConstructor",
  "PMD.CommentSize",
  "PMD.LongVariable",
  "PMD.LawOfDemeter"
})
public class AccountInformation {

  /** Shared string value for header and footer templates, marked as final to ensure immutable. */
  private final String headerFooterTemplate =
      new StringBuilder()
          .append(
              "======================================================================================\n")
          .append(
              "======================================================================================\n")
          .toString();

  /** Encapsulated getter for Header. */
  public String getHeader() {
    return headerFooterTemplate;
  }

  /** Encapsulated getter for Footer. */
  public String getFooter() {
    return headerFooterTemplate;
  }

  /** Encapsulated getter for Access Token. */
  public String getAccessToken() throws IOException {

    String returnValue;

    final ApplicationProperties applicationProperties = ApplicationProperties.getInstance();

    final URL url = new URL("https://ob.sandbox.natwest.com/token");

    final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    connection.setRequestMethod("POST");
    connection.setRequestProperty("accept", "application/json");
    connection.setDoOutput(true);

    final String queryString =
        String.format(
            "grant_type=client_credentials&client_id=%s&client_secret=%s",
            applicationProperties.getClientID(), applicationProperties.getClientSecret());

    try (OutputStream outputStream = connection.getOutputStream()) {
      final byte[] input = queryString.getBytes("utf-8");
      outputStream.write(input, 0, input.length);
    }

    try (InputStream responseStream = connection.getInputStream(); ) {
      final ObjectMapper mapper = new ObjectMapper();
      returnValue = mapper.readValue(responseStream, AccessToken.class).access_token;
    }

    return returnValue;
  }
}
