package com.natwest.ai.exercise.common;

public interface ConsoleWriteableAccountInformation {

  public String getHeader();

  public String getContent();

  public String getFooter();
}
