package com.natwest.ai.exercise.common;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class ConsoleWriter {

  public void writeAccountInformation(ConsoleWriteableAccountInformation accountInformation) {

    final OutputStreamWriter output_stream_writer = new OutputStreamWriter(System.out);
    final BufferedWriter buffered_writer = new BufferedWriter(output_stream_writer);

    try {
      buffered_writer.write(accountInformation.getHeader());
      buffered_writer.write(accountInformation.getContent());
      buffered_writer.write(accountInformation.getFooter());
      buffered_writer.close();
      output_stream_writer.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
