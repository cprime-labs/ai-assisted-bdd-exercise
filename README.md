# ai-assisted-bdd-exercise

## Install and Run Pre-Commit

```shell

pip install pre-commit && pre-commit run --all

```

## Run Maven Tests

```shell

make mvn/test

```

## View Maven Test Coverage

```shell

cd target/site/jacoco && python -m http.server 8000

```

## Install Postman

```shell

curl -o- "https://dl-cli.pstmn.io/install/linux64.sh" | sh && postman login

```


